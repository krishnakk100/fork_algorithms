CC=clang
CFLAGS=-g3 -c -Wall -O2
LDFLAGS=-lm
SOURCES=main.c lists.c algorithms.c string_builder.c heapsort.c
OBJECTS=$(SOURCES:.c=.o)
EXECUTABLE=main

TESTDIR=tests

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

%.o: %.c
	$(CC) $(CFLAGS) $< -o $@

check: clean
	$(MAKE) -C $(TESTDIR) clean
	$(MAKE) -C $(TESTDIR) check

clean:
	rm -rf $(OBJECTS) $(EXECUTABLE)

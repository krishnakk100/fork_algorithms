/*
 *-------------------------------------------------------------------------
 * algorithms.c
 *	Some algorithms implementation
 *-------------------------------------------------------------------------
 */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdint.h>
#include "algorithms.h"

#define RANDOM_MODULUS    (2147483647)
#define RANDOM_MULTIPLIER (1103515245)
#define RANDOM_INCREMENT  (12345)

static unsigned long int next = 1;

/*
 * linear congruential generator implementation
 * https://en.wikipedia.org/wiki/Linear_congruential_generator
 */
int rand()
{
	next = ( RANDOM_MULTIPLIER * next + RANDOM_INCREMENT ) % RANDOM_MODULUS;
	return next;
}

void srand(unsigned int seed)
{
	next = seed;
}

int rand_between(int min, int max)
{
	double num = rand() / (double) RANDOM_MODULUS;
	return (int) min + num * (max - min + 1);
}

void swap(int array[], int i, int j)
{
	int tmp  = array[i];
	array[i] = array[j];
	array[j] = tmp;
}

/*
 * modern version of the Fisher–Yates shuffle implementation
 * https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle
 */
void shuffle(int array[], int size)
{
	int n;
	for (int i = 0; i < size - 1; ++i)
	{
		n = rand_between(i, size - 1);
		swap(array, i, n);
	}
}

/*
 * find great common divisor using rule:
 * 	GCD(A, B) = GCD(B, A mod B)
 * max time complexity is O(log(B))
 *
 * https://en.wikipedia.org/wiki/Greatest_common_divisor
 */
int gcd(int a, int b)
{
	int r;

	while (b != 0)
	{
		r = a % b;
		a = b;
		b = r;
	}

	return a;
}

/*
 * find least common multiple through GCD
 * https://en.wikipedia.org/wiki/Least_common_multiple
 */
int lcm(int a, int b)
{
	return (a * b) / gcd(a, b);
}


/*
 * exponentiation by squaring method implementation
 * https://en.wikipedia.org/wiki/Exponentiation_by_squaring
 */
int ipow(int a, unsigned int p)
{
	int ret = 1;

    while (p)
    {
        if (p & 1) ret *= a;

        p >>= 1;
        a *= a;
    }

    return ret;
}

/*
 * modular exponentiation implementation
 * memory-efficient method
 *
 * using rules:
 * 	c mod m = (a * b) mod m
 * 	c mod m = [(a mod m) * (b mod m)] mod m
 * https://en.wikipedia.org/wiki/Modular_exponentiation
 */
int mod_ipow(int a, unsigned int p, unsigned int m)
{
	if (m == 1) return 0;

	int ret = 1;
	for (int i = 0; i < p; i++)
		ret = (ret * a) % m;

	return ret;
}

/*
 * simple factorization method implementaion
 * time complexity is O(sqrt(N))
 * https://en.wikipedia.org/wiki/Trial_division
 *
 * return array of factors
 * write returned array length to params length
 */
int * factorization(int num, int * length)
{
	int max_size  = (int) log2((double) num);
	int * factors = (int*) malloc(max_size * sizeof(int));
	*length = 0;

	// check dividing by 2
    for (; !(num & 1); num >>= 1)
		factors[(*length)++] = 2;

	int max_factor = sqrt(num);

	// find odd factors
	for (int factor = 3; factor < max_factor; factor += 2)
	{
		// check dividing by factor
		for (; num % factor == 0; num /= factor, max_factor = sqrt(num))
			factors[(*length)++] = factor;
	}

	// if there is a rest add it
	if (num > 1) factors[(*length)++] = num;

	return factors;
}

/*
 * Sieve of Eratosthenes method implementation
 * time complexity is O(N log(log(N)))
 * https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
 *
 * return array of factors
 * write returned array length to params length
 */
int * find_primes(int max, int * length)
{
	*length = max;

	char * is_composite = (char*) malloc(*length);
	for (int i = 0; i <= max; i++)
		is_composite[i] = 0;

	// exclude even numbers
	for (int i = 4; i <= max; i += 2) {
		is_composite[i] = 1;
		(*length)--;
	}

	int next = 3;
	int stop = sqrt(max);

	while (next <= stop) {
		// exclude numbers multiple by next prime number
		for (int i = next * next; i <= max; i += next) {
			// find new composite number
			if (!is_composite[i]) {
				is_composite[i] = 1;
				(*length)--;
			}
		}

		next += 2;
		// find next prime excluded even numbers
		while (next < stop && is_composite[next]) next += 2;
	}

	(*length)--;
	int * primes = (int*) malloc((*length) * sizeof(int));

	for (int i = 2, j = 0; i <= max; i++)
		if (!is_composite[i]) primes[j++] = i;

	free(is_composite);
	return primes;
}

/*
 * Fermat primality test implementation:
 * 	number p is prime if n^(p - 1) mod p = 1; (1 <= n < p)
 * https://en.wikipedia.org/wiki/Fermat_primality_test
 */
bool is_prime(int num, double max_error_probability)
{
	int k = log2(1.0 / max_error_probability);

	int rand;
	while (k--)
	{
		rand = rand_between(1, num - 1);
		if (mod_ipow(rand, num - 1, num) != 1)
			return 0;
	}
	return 1;
}

/*
 * find random prime number with num_digits digits
 * using Fermat primality test
 */
int find_prime(int num_digits, double max_error_probability) {
	int min = ipow(10, num_digits - 1);
	int max = ipow(10, num_digits) - 1;

	int num;
	while (1)
	{
		num = rand_between(min, max);
		if (is_prime(num, max_error_probability))
			return num;
	}
};


/*
 * numerical integration
 * rectangle method implementation
 * https://en.wikipedia.org/wiki/Rectangle_method
 */
double rectangle_rule(funptr_t fun, double xmin, double xmax, int intervals)
{
	double dx = (xmax - xmin) / intervals;
	double area = 0.0;

	for (double x = xmin; x < xmax; x += dx)
		area += dx * fun(x + dx / 2);

	return area;
}

/*
 * numerical integration using trapezoidal rule
 * https://en.wikipedia.org/wiki/Trapezoidal_rule
 */
double trapezoidal_rule(funptr_t fun, double xmin, double xmax, int intervals)
{
	double dx = (xmax - xmin) / intervals;
	double area = 0.0;

	for (double x = xmin; x < xmax; x += dx)
		area += dx * (fun(x) + fun(x + dx)) / 2;

	return area;
}

/*
 * adaptive quadrature
 * https://en.wikipedia.org/wiki/Adaptive_Simpson's_method
 */
double adaptive_method(funptr_t fun,
					   double xmin,
					   double xmax,
					   int intervals,
					   double max_slice_error)
{
	double dx = (xmax - xmin) / intervals;
	double area = 0.0;

	for (double x = xmin; x < xmax; x += dx)
		area += slice_area(fun, x, x + dx, max_slice_error);

	return area;
}

double slice_area(funptr_t fun,
                  double x1,
                  double x2,
                  double max_slice_error)
{
	double y1 = fun(x1);
	double y2 = fun(x2);
	// x mean
	double xm = (x1 + x2) / 2;
	double ym = fun(xm);

	// calc big trapezoidal area and two small trapezoidal area
	double area12  = (x2 - x1) * (y1 + y2) / 2;
	double area1m  = (xm - x1) * (y1 + ym) / 2;
	double aream2  = (x2 - xm) * (ym + y2) / 2;
	double area1m2 = area1m + aream2;

	// relative error
	double error = (area1m2 - area12) / area12;

	if (fabs(error) < max_slice_error)
		return area1m2;

	return slice_area(fun, x1, xm, max_slice_error) +
		slice_area(fun, xm, x2, max_slice_error);
}

/*
 * monte carlo integration implementation
 * 	figure is function that return 1 if (x, y) belonging to area and 0 if not
 * https://en.wikipedia.org/wiki/Monte_Carlo_integration
 */
double monte_carlo_method(figptr_t figure,
						  double xmin,
						  double xmax,
						  double ymin,
						  double ymax,
						  int points)
{
	int figure_darts = 0;

	double randx, randy;
	int t = points;

	while (t--)
	{
		randx = rand_between(xmin, xmax);
		randy = rand_between(ymin, ymax);

		figure_darts += figure(randx, randy);
	}

	return (xmax - xmin) * (ymax - ymin) * figure_darts / points;
}

/*
 * Newton's method in optimization:
 * 	Xn+1 = Xn - f(Xn) / f'(Xn)
 * https://en.wikipedia.org/wiki/Newton%27s_method_in_optimization
 */
double newton_method(funptr_t f,
                     funptr_t dfdx,
                     double x0,
                     double max_error)
{
#define NEWTON_MAX_ITER (10000)

    double x = x0;
	double y;

	int iter = NEWTON_MAX_ITER;
	// if something went wrong we would stop
	while (iter--)
	{
		y = f(x);
		if (fabs(y) < max_error) break;

		x = x - y / dfdx(x);
	}

	return x;
}

/*
 * -------------------------------------------------------------------------
 * algorithms.h
 *	Integrated some algorithms
 * -------------------------------------------------------------------------
 */

#ifndef ALGORITHMS_H
#define ALGORITHMS_H

#include <stdbool.h>

int rand   ();
void srand (unsigned int seed);

int rand_between (int min, int max);
void shuffle     (int array[], int size);

int gcd (int a, int b);
int lcm (int a, int b);

int ipow 	 (int a, unsigned int p);
int mod_ipow (int a, unsigned int p, unsigned int m);

int * factorization (int num, int * length);
int * find_primes   (int max, int * length);

int   find_prime   (int num_digits, double max_error_probability);
bool  is_prime     (int num, 		double max_error_probability);


/*
 * Numerical integration
 * TODO: instead of numbers of intervals we want to set maximum integration error
 */
typedef double (*funptr_t)(double);

double rectangle_rule   (funptr_t fun, double xmin, double xmax, int intervals);
double trapezoidal_rule (funptr_t fun, double xmin, double xmax, int intervals);
double adaptive_method  (funptr_t fun,
					     double xmin,
					     double xmax,
					     int intervals,
					     double max_slice_error);

double slice_area(funptr_t fun, double x1, double x2, double max_slice_error);

/*
 * Monte Carlo
 */
typedef char (*figptr_t)(double, double);
double monte_carlo_method (figptr_t figure,
						   double xmin,
						   double xmax,
						   double ymin,
						   double ymax,
						   int points);

/*
 * Functions optimization
 */
double newton_method(funptr_t f, funptr_t dfdx, double x0, double max_error);

#endif /* ALGORITHMS_H */

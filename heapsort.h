/*
 *-------------------------------------------------------------------------
 * headsort.h
 *  Integrated HeapSort
 *-------------------------------------------------------------------------
 */

typedef unsigned int uint;

#define child_left_idx(i)  (2 * i + 1)
#define child_right_idx(i) (2 * i + 2)
#define parent_idx(i)      ((i - 1) / 2)

#define max_idx(arr, i, j) (arr[j] > arr[i] ? j : i)

void sift_up(int * arr, uint idx);
void sift_down(int * arr, const uint len);

void heapify(int * arr, const uint len);
void heapsort(int * arr, const uint len);

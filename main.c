#include <time.h>
#include <stdio.h>
#include <stdlib.h>

#include "algorithms.h"
#include "lists.h"
#include "string_builder.h"
#include "heapsort.h"

void string_builder_demo();

void print_iarray(int * array, int size)
{
	for (int i = 0; i < size; i++)
		printf("%d ", array[i]);
	printf("\n");
}

// f(x) = x;
double f1(double x) { return x; }
// f(x) = x^2;
double f2(double x) { return x *x; }
// f(x) = x^3 - 5x + 10
double f3(double x) { return x * x * x - 5 * x + 10; }
// f(x) = 3x^2 - 5
double df3dx(double x) { return 3 * x * x - 5; }
// x^2 + y^2 <= 4
char circle2(double x, double y) { return (x * x + y * y) <= 4 ? 1: 0; }

int _main(int argc, char const *argv[])
{
	srand((unsigned int) time(NULL));
	int num = rand();
	printf("random number: %d\n", num);

	int arr[] = { 0, 1, 2, 3, 4, 5 };
	shuffle(arr, 6);
	print_iarray(arr, 6);

	printf("number 13 is prime(p_err = 0.5)   ? %d\n", is_prime(13, 0.5));
	printf("number 13 is prime(p_err = 0.001) ? %d\n", is_prime(13, 0.001));

	printf("prime number with 3 digits is %d\n", find_prime(3, 0.001));
	printf("prime number with 5 digits is %d\n", find_prime(5, 0.00001));

	// exact  answer is 50
	printf("integral = %f\n", rectangle_rule(f1, 0.0, 10.0, 100));
	// exact  answer is 252
	printf("integral = %f\n", rectangle_rule(f2, -3, 9, 10));
	// 14
	printf("integral (trapezoidal_rule) = %f\n", trapezoidal_rule(f3, 0, 2, 1000));
	printf("integral (rectangle_rule)   = %f\n", rectangle_rule(f3, 0, 2, 1000));
	printf("integral (adaptive_method)  = %f\n", adaptive_method(f3, 0, 2, 100, 0.001));

	// circle area is 12.566..
	printf("circle area is %f\n", monte_carlo_method(circle2, -2, 2, -2, 2, 1000));

	// f(x) = 0 when x = -2.9005
	printf("f(x) = x^3 - 5x + 10 = 0 when x = %f\n", newton_method(f3, df3dx, -4.0, 0.01));

	return 0;
}

int main() {
	int size = 50000;
	int* v = (int *) malloc(sizeof(int) * size);
	int i;

	float time_global = 0;

	for (i = 0; i < 10000; i++)
	{
		for (int j = 0; j < size; j++)
			v[j] = rand();

		clock_t t1 = clock();
		heapsort(v, size);
		clock_t t2 = clock();

		float diff = ((float)(t2 - t1) / 1000000.0F ) * 1000;
		time_global += diff;
	}

	printf("%f\n", time_global / 1000);

	free(v);
}
